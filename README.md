Injector <Process Name> <Dll Path> [OPTIONS]

# OPTIONS
	
	-h 					Print this help text and exit
	-l <Path> [args]	Try to launch process with the args if he can not find one (precise -noarg or empty arg if no args)

# TODO

	Add other injection type (code cave / SetWindowsHook / ...)
