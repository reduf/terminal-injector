#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <tlhelp32.h>

#define OPEN_PROCESS_FLAG PROCESS_CREATE_THREAD | PROCESS_VM_WRITE | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION

#define ERR_ARGUMENT        0x11
#define ERR_DLL_PATH        0x12
#define ERR_OPEN_PROCESS    0x14
#define ERR_NO_PROCESS      0x18
#define ERR_LAUNCH_PROCESS  0x20

#define INJECT_DLL          0x001
#define LAUNCH_PROCESS      0x002
#define SET_PRIVILEGE       0x004
#define REMOTE_INJECTION    0x100
#define DEFAULT_FLAG INJECT_DLL | SET_PRIVILEGE | REMOTE_INJECTION

#define DEFAULT_OUT stdout

typedef unsigned int uint;

uint    GetPid(char *procName);
HMODULE GetDllHandle(uint pid, char *name);
BOOL    SetPrivilege(HANDLE process, BOOL enable);
int     LaunchProcess(const char *path, const char *arg, uint* threadId);

HMODULE kernel32 = NULL;

HMODULE InjectDll(HANDLE proc, char *dllPath)
{
    FARPROC LoadLibraryAdr = GetProcAddress(kernel32, "LoadLibraryA");

    uint length = strlen(dllPath);
    LPVOID mem = VirtualAllocEx(proc, NULL, length + 1, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);

    WriteProcessMemory(proc, mem, dllPath, length + 1, NULL);
    HANDLE thread = CreateRemoteThread(proc, NULL, 0, (LPTHREAD_START_ROUTINE)LoadLibraryAdr, mem, 0, NULL);
    WaitForSingleObject(thread, INFINITE);

    HMODULE module = NULL;
    GetExitCodeThread(thread, (LPDWORD)&module);

    VirtualFreeEx(proc, mem, 0, MEM_RELEASE);
    CloseHandle(thread);

    return module;
}

BOOL UnloadDll(HANDLE proc, HMODULE module)
{
    FARPROC FreeLibraryAdr = GetProcAddress(kernel32, "FreeLibrary");

    HANDLE thread = CreateRemoteThread(proc, NULL, 0, (LPTHREAD_START_ROUTINE)FreeLibraryAdr, module, 0, NULL);
    WaitForSingleObject(thread, INFINITE);

    BOOL success;
    GetExitCodeThread(thread, (LPDWORD)&success);

    CloseHandle(thread);
    return success;
}

int main(int argc, char *argv[])
{
    char *procPath = NULL, *procName = NULL, *procArg = "";
    char *dllPath = NULL, *dllName = NULL;
    char flag = DEFAULT_FLAG;

    HANDLE proc = NULL;

    if (argc < 2)
    {
        fputs("You don't have enough argument. Make sure to past the process name and the path to a dll.\n", DEFAULT_OUT);
        return ERR_ARGUMENT;
    }

    procName = argv[1];
    dllPath = argv[2];

    if (!isalpha(dllPath[0]) || dllPath[1] != ':')
    {
        fputs("The dll path should be an absolute path.\n", DEFAULT_OUT);
        return ERR_DLL_PATH;
    }

    dllName = strrchr(dllPath, '/');
    if (!dllName) dllName = strrchr(dllPath, '\\');
    if (!dllName) return ERR_DLL_PATH;
    dllName = dllName + 1;

    for (int i = 3; i < argc; i++)
    {
        if ( !strcmp(argv[i], "-u") )
        {
            flag -= INJECT_DLL;
        }
        else if ( !strcmp(argv[i], "-l") )
        {
            flag |= LAUNCH_PROCESS;
            procPath = argv[++i];
            if (++i < argc)
            {
                if (strcmp(argv[i], "-noarg"))
                {
                    procArg = argv[i];
                }
            }
        }
    }

    uint pid = GetPid(procName);
    uint tid = 0;

    if (!pid)
    {
        if (flag & LAUNCH_PROCESS)
        {
            if (!(pid = LaunchProcess(procPath, procArg, &tid)))
            {
                fputs("Unable to start the process.\n", DEFAULT_OUT);
                return ERR_LAUNCH_PROCESS;
            }
        }
        else
        {
            fputs("The process doesn't exist. You can use \"-l exePath/exeName.exe\" argument for launch the process then inject.\n", DEFAULT_OUT);
            return ERR_NO_PROCESS;
        }
    }

    if (flag & SET_PRIVILEGE) SetPrivilege(GetCurrentProcess(), TRUE);

    proc = OpenProcess(OPEN_PROCESS_FLAG, FALSE, pid);

    kernel32 = LoadLibrary("kernel32.dll");

    if (!proc)
    {
        fputs("Could'nt open the process, you may have to launch the Injector in admin mod.\n", DEFAULT_OUT);
        return ERR_OPEN_PROCESS;
    }


    if (flag & INJECT_DLL)
    {
        HMODULE module = InjectDll(proc, dllPath);
        
        HANDLE thread = OpenThread(THREAD_SUSPEND_RESUME, FALSE, tid); // Resume thread after inject dll
        if (thread)
        {
            ResumeThread(thread);
            CloseHandle(thread);
        }

        fputs("Do you want to keep the dll injected ? (Y/n) : ", stdout);
        int answer = getchar();

        if (answer == 'n' || answer == 'N')
        {
            UnloadDll(proc, module);
        }
    }
    else
    {
        HMODULE module = GetDllHandle(pid, dllName);
        UnloadDll(proc, module);
    }


    CloseHandle(proc);

    return 0;
}

uint GetPid(char *procName)
{
    HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    PROCESSENTRY32 buffer;
    buffer.dwSize = sizeof(PROCESSENTRY32);

    do
    {
        if (!strcmp(buffer.szExeFile, procName))
        {
            uint pid = buffer.th32ProcessID;
            CloseHandle(hSnap);
            return pid;
        }
    } while(Process32Next(hSnap, &buffer));
    CloseHandle(hSnap);

    return 0;
}

HMODULE GetDllHandle(uint pid, char *name)
{
    HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);
    MODULEENTRY32 buffer;
    buffer.dwSize = sizeof(MODULEENTRY32);

    do
    {
        if (!strcmp(buffer.szModule, name))
        {
            HMODULE handle = buffer.hModule;
            CloseHandle(hSnap);
            return handle;
        }
    } while(Module32Next(hSnap, &buffer));
    CloseHandle(hSnap);

    return NULL;
}

BOOL SetPrivilege(HANDLE process, BOOL enable)
{
    TOKEN_PRIVILEGES tp;
    LUID luid;
    HANDLE hTok = NULL;

    OpenProcessToken(process, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY | TOKEN_READ, &hTok);

    if (!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luid))
    {
        printf("LookupPrivilegeValue error: %u\n", GetLastError());
        return FALSE;
    }

    tp.PrivilegeCount = 1;
    tp.Privileges[0].Luid = luid;
    tp.Privileges[0].Attributes = enable ? SE_PRIVILEGE_ENABLED : 0;

    if (!AdjustTokenPrivileges(hTok, FALSE, &tp, sizeof(TOKEN_PRIVILEGES), NULL, NULL))
    { 
        printf("AdjustTokenPrivileges error: %u\n", GetLastError());
        return FALSE;
    }

    CloseHandle(hTok);

    return TRUE;
}

int LaunchProcess(const char *path, const char *arg, uint *threadId)
{
    char *launchCmd = malloc(strlen(path) + strlen(arg) + 2);
    strcpy(launchCmd, path);
    strcat(launchCmd, " ");
    strcat(launchCmd, arg);

    PROCESS_INFORMATION pInfo = {0};
    STARTUPINFO info = {0}; // Might add future use
    info.cb = sizeof(info);

    BOOL bRet = CreateProcess(NULL, launchCmd, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW | CREATE_SUSPENDED, NULL, NULL, &info, &pInfo);

    if (threadId)
        *threadId = pInfo.dwThreadId;
    else
        ResumeThread(pInfo.hThread);

    free(launchCmd);
    CloseHandle(pInfo.hThread);
    CloseHandle(pInfo.hProcess);

    return pInfo.dwProcessId;
}

void ShowHelp()
{

}
